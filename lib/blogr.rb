require "blogr/engine"

module Blogr
  mattr_accessor :meta_title, :meta_description, :meta_keywords
end
